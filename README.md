# Introduction

This project creates a docker image with (open)jdk 7 and maven 3 installed.

The image can be used to run maven builds.

This repository is mirrored to https://gitlab.com/sw4j-net/jdk7-maven3

## Deprecation

As Java 7 is deprecated this repository and the generated images will be removed in January 2019.
